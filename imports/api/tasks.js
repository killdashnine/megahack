import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Random } from 'meteor/random';

export const Tasks = new Mongo.Collection('tasks');

import {Profiles} from './profiles';

if (Meteor.isServer) {

    Meteor.publish('tasks', function() {

        return Tasks.find();
    });
}

Meteor.methods({
    'tasks.accept'(id, solutionId, userId) {
        // console.log(id, solutionId, userId);

        check(id, String);
        check(solutionId, String);
        check(userId, String);

        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        let task = Tasks.findOne( { _id: id } );

        // console.dir(task);

        if(task.customer._id !== this.userId)
            throw new Meteor.Error('not-allowed');

        let solution = task.solutions.filter(solution => solution._id === solutionId)[0];

        solution.accepted = true;

        task.contractor = { _id: userId };

        Tasks.update({_id: id}, task);

        // { _id: id }, { $set: { "solutions._id", contractor: {_id: userId} } }
    },

    'tasks.insert'(name, tags, desc, deadline, price) {
        check(name, String);
        check(tags, [String]);
        check(desc, String);
        check(deadline, Date);
        check(price, Number);

        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        let profile = Profiles.findOne( {userId: this.userId} ) || {};

        Tasks.insert({name, desc, tags, deadline, customer: { _id: this.userId, name: profile.name || '' }, createdAt: new Date(), price});
    },

    'tasks.solutions.insert'(id, message, help) {
        check(id, String);
        check(message, String);
        check(help, Boolean);

        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        let task = Tasks.findOne(id);

        let offered = (task.solutions || []).filter(solution => (solution.contractor||{})._id === this.userId).length;

        if(offered) {
            throw new Meteor.Error('already offered solution');
        }

        Tasks.update({_id: id},
            {
                $push: {
                        solutions: Object.assign({
                            _id: Random.id(),
                            message,
                            contractor: {
                                _id: this.userId
                            }}, help ? {
                            helpers: {}
                        } : {})

                }
            });
    },
    'tasks.solutions.points.insert'(taskId, solutionId, goal, time){
        check(taskId, String);
        check(solutionId, String);
        check(goal, String);
        check(time, Date);

        if (! this.userId)
            throw new Meteor.Error('not-authorized');

        let task = Tasks.findOne(taskId);

        let solution = task.solutions.filter( solution => solution._id === solutionId ) [0];

        if (   this.userId !== (solution.contractor || {})._id
            && this.userId !== (solution.mentor || {})._id)
            throw new Meteor.Error('not-allowed');

        if(!solution.points) solution.points = [];

        solution.points.push({
            _id: Random.id(),
            time,
            goal,
            userId: this.userId
        });

        Tasks.update({ _id: taskId }, task);

        // Tasks.update({ _id: taskId },
        //     {
        //         $push: {
        //             'solutions.$[solution].points': {
        //                 _id: Random.id(),
        //                 time,
        //                 goal,
        //                 userId: this.userId
        //             }
        //         }
        //     },
        //     {
        //         arrayFilters: [ { 'solution._id': solutionId } ],
        //         upsert: false });
    }
});
