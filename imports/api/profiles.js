import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Profiles = new Mongo.Collection('profiles');

if (Meteor.isServer) {

    Meteor.publish('profiles', function() {

        return Profiles.find();
    });
}

Meteor.methods({
    'profiles.upsert'(name, desc, role) {
        check(name, String);
        check(desc, String);
        check(role, String);

        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        const roles = {
            contractor: 'Исполнитель/Ученик',
            customer: 'Заказчик',
            mentor: 'Наставник'
        };

        if(!name)
            throw new Meteor.Error('name-not-specified');

        if(!Object.keys(roles).includes(role))
            role = 'contractor';

        if(!desc)
            desc = roles[role];

        Profiles.upsert({userId: this.userId}, { userId: this.userId, name, desc, role });
    }
});
