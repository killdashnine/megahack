
export function shiftDate(date = new Date(), day = 0, hour = 0, minute = 0) {
    let d = new Date();
    d.setTime(date.getTime() + 1000*60*minute + 1000*60*60*hour + 1000*60*60*24*day);
    return d;
}

/**
 * d/h:m to millis
 * @param day 1 - monday, 7 - sunday
 * @param hour 0-60
 * @param minute 0-60
 * @returns {number}
 */
export function relativeMillis(day, hour, minute) {
    let d = new Date();
    d.setDate(0);
    return shiftDate(d, day, hour, minute).getTime();
}

let days = {
    'monday': 0,
    'tuesday': 1,
    'wednesday': 2,
    'thursday': 3,
    'friday': 4,
    'saturday': 5,
    'sunday': 6,
};

Object.entries(days).map(pair => days[pair[1]] = pair[0]);

// Days become mirrored: week[0] === 'monday' && week['monday'] === 0;
export const week = days;
