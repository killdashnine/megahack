import React, {Component} from "react";
import {Button} from 'reactstrap';
import {autobind} from 'core-decorators';

import TaskList from '../task/TaskList';
import TagFilterSide from '../sidebars/TagFilterSide';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Tasks} from "../../api/tasks";

class TasksPage extends Component {

    state = {chosenTag: null};

    @autobind
    setFilter(tag) {
        this.setState({chosenTag: tag});
    }

    @autobind
    resetFilter() {
        this.setFilter(null);
    }

    @autobind
    filteredTasks() {

        if(!this.props.tasks) return [];

        let tags = this.state.chosenTag;

        if(!tags)
            return this.props.tasks;

        tags = Array.isArray(tags) ? tags : [tags];

        if(tags.length === 0)
            return this.props.tasks;

        const tagSet = new Set(tags);

        return this.props.tasks.filter(task => task.tags.filter(tag => tagSet.has(tag)).length);
    }

    render(){

        return (
            <div className="container">

                <div className="a-list">

                    <div className="row">
                        <div className="col-9">
                            <TaskList tasks={this.filteredTasks()} incompleteCount={this.props.incompleteCount} />
                        </div>
                        <div className="col-3">
                            <TagFilterSide tags={this.props.tags} onTagChosen={this.setFilter}/>

                            {/*{ this.state.chosenTag
                                ? <Button onClick={this.resetFilter} className="w-100" color="primary">Отмена</Button>
                                : ''}*/}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTracker(() => {

    Meteor.subscribe('tasks');

    const tasks = Tasks.find({}).fetch().reverse();

    let tags = [];

    tasks.forEach(task => tags = tags.concat(task.tags||[]));

    tags = [ ... new Set(tags)];

    return {
        tasks,
        tags,
        incompleteCount: Tasks.find({ completed: { $eq: false } }).count(),
    };
})(TasksPage);
