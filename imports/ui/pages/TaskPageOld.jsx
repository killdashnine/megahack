import React, {Component} from 'react';

import { Badge, Form, FormGroup, Input, Button} from 'reactstrap';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Tasks} from "../../api/tasks";

class TaskPage extends Component {

    render() {

        let task = this.props.task;

        if(!task)
            return (<p>Task not found.</p>);

        return (
            <div>
                <h3 key="name"> { task.name } </h3>

                { task.tags.map(tag => <Badge color="secondary" className="m-1" key={tag}>{tag}</Badge>) }

                <h5>Описание заказа</h5>

                <p>{ task.desc }</p>

                {task.deadline ?
                    <div key="deadline">
                        <h5>Дата размещения</h5>

                        <p>{ String(task.deadline) }</p>
                    </div>
                    : ''}

                {task.price ?
                    <div key="price">
                        <h5>Бюджет</h5>
                        <p>{ task.price + " рублей" }</p>
                    </div> : ''}

                {task.customer ?
                    <div key="customer">
                        <h5>Заказчик</h5>
                        <p>{ task.customer.name }</p>
                    </div> : ''}

                <h3 key="solution">Отклики</h3>
                <Form>
                    <FormGroup>
                        <Input
                            type="textarea"
                            placeholder="Напишите отклик"
                            name="text"
                        />
                        <Button className="float-right">Отправить</Button>
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('tasks');

    let task = Tasks.findOne({
        _id: props.match.params.id
    });

    return {
        task
    };
})(TaskPage);
