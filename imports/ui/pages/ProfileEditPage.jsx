import React, {Component} from 'react';

import { UncontrolledAlert, Label, Form, FormGroup, Input, Button} from 'reactstrap';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";

import {autobind} from 'core-decorators';

import {Redirect} from "react-router";

import {Profiles} from '../../api/profiles';

function safe(f){
    try {
        return [f(), null];
    }
    catch (e) {
        return [null, e];
    }
}

class ProfileEditPage extends Component {

    state = {
        done: false,
        name: "",
        desc: "",
        role: "contractor",
        init: false,
        error: null
    };

    @autobind
    handleSubmit(event) {

        event.preventDefault();

        const name = this.state.name.trim();
        const desc = this.state.desc.trim();
        const role = this.state.role;

        Meteor.call('profiles.upsert', name, desc, role, (error, result) => {
            if(error)
                this.setState({error: error.error});
            else
                this.setState({done: true});
        });

        // try{
        //
        // } catch (error) {
        //
        // }

        // safe()


    }

    @autobind
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    componentWillReceiveProps(nextProps){
        this.initialize(nextProps);
    }

    componentDidMount(){
        this.initialize(this.props);
    }

    initialize(props){

        if(!this.state.init && props.profile) {

            let profile = props.profile || {};

            this.setState(Object.assign({ init: true, error: null }, profile));
        }

        if(this.state.init && !props.profile) {

            let profile = {name: "", desc: "",  role: "contractor"};

            this.setState(Object.assign({ init: false, error: null }, profile));
        }
    }

    render() {

        if(!this.props.user)
            return (<p>Вам нужно авторизироваться в системе, чтобы изменить профиль.</p>);

        return (
            this.state.done
                ? <Redirect to="/task"/>
                : <div className="card">

                    <div className="card-body">
                        <h4 className="mt-0 mb-3">Редактирование профиля</h4>

                        { this.state.error === 'name-not-specified'
                            ? <UncontrolledAlert color="danger">Имя и роль - обязательные поля</UncontrolledAlert> : null }

                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <Label for="input1">Имя</Label>
                                <Input
                                    ref="name"
                                    type="text"
                                    id="input1"
                                    className="form-control"
                                    placeholder="Иван Иванов"
                                    name="name"
                                    value={this.state.name}
                                    onChange={this.handleInputChange} />
                            </FormGroup>

                            <FormGroup>
                                <Label for="textarea1">Описание</Label>
                                <Input
                                    ref="desc"
                                    type="textarea"
                                    id="textarea1"
                                    className="form-control"
                                    placeholder="Новосибирск, 20 лет, Web-разработчик"
                                    rows="3"
                                    name="desc"
                                    value={this.state.desc}
                                    onChange={this.handleInputChange} />
                            </FormGroup>

                            <FormGroup>
                                <Label for="input3">Роль</Label>
                                <Input
                                    ref="role"
                                    type="select"
                                    id="input3"
                                    className="form-control"
                                    name="role"
                                    value={this.state.role}
                                    onChange={this.handleInputChange} >

                                    <option value="contractor">Исполнитель / Ученик</option>
                                    <option value="customer">Заказчик</option>
                                    <option value="mentor">Наставник</option>
                                </Input>
                            </FormGroup>

                            <div className="form-row align-items-center justify-content-end">
                                <div className="col-auto">
                                    <Button type="submit" className="btn btn-primary mb-2">Сохранить</Button>
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
        );
    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('profiles');

    return {
        user: Meteor.user(),
        profile: Profiles.findOne({userId: Meteor.userId()})
    };
})(ProfileEditPage);
