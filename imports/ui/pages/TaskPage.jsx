import React, {Component} from 'react';

import { Badge, Form, FormGroup, Input, Button} from 'reactstrap';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Tasks} from "../../api/tasks";
import Task from '../task/Task';
import TaskSolutionOffer from '../task/TaskSolutionOffer';
import TaskSolutionList from '../task/TaskSolutionList';

import ProfileSide from '../sidebars/ProfileSide';

class TaskPage extends Component {

    render() {

        let task = this.props.task;

        if(!task)
            return (<p>Task not found.</p>);

        return (
            <div className="container">
                <div className="i-order">
                    <div className="row">
                        <div className="col-8">
                            <Task task={task}/>
                            { this.props.user && this.props.user._id !== task.customer._id
                                ? <TaskSolutionOffer task={task} />
                                : '' }
                            <TaskSolutionList solutions={task.solutions} extended={false} taskId={task._id} />
                        </div>
                        <div className="col-4">
                            <ProfileSide header={"Заказчик"} userId={task.customer._id}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('tasks');

    return {
        task: Tasks.findOne({_id: props.match.params.id}),
        user: Meteor.user()
    };
})(TaskPage);
