import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import { Alert, Badge, Form, FormGroup, Input, Button} from 'reactstrap';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";

import {autobind} from 'core-decorators';

import {Redirect} from "react-router";
import {Profiles} from "../../api/profiles";
import {Link} from "react-router-dom";

class TaskCreatePage extends Component {

    state = {done: false};

    @autobind
    handleSubmit(event) {

        event.preventDefault();

        const name      = ReactDOM.findDOMNode(this.refs.name).value.trim();
        const tagLine   = ReactDOM.findDOMNode(this.refs.tagLine).value;
        const desc      = ReactDOM.findDOMNode(this.refs.desc).value.trim();
        const date      = ReactDOM.findDOMNode(this.refs.deadline).value;
        const price     = Number(ReactDOM.findDOMNode(this.refs.price).value || 0);

        let tags = tagLine.split(",").map(s => s.trim()).filter(s => !!s);
        let uniqueTags = [ ... new Set(tags)];
        let deadline = new Date(date);

        // console.log(name, uniqueTags, desc, deadline, price);

        if(name.length > 0 && desc.length > 0)
            Meteor.call('tasks.insert', name, uniqueTags, desc, deadline, price);

        this.setState({done: true});
    }

    render() {

        let user = this.props.user;

        if(!user)
            return (<p>Вам нужно авторизироваться в системе, чтобы создать заказ.</p>);

        return (
            this.state.done
                ? <Redirect to="/task"/>
                : <div className="card">

                    <div className="card-body">
                        <h4 className="mt-0 mb-3">Создание нового заказа</h4>

                        { !this.props.hasProfile
                            ? <Alert color="danger">У Вас ещё нет профиля. <Link to="/profile">Создайте его.</Link></Alert>
                            : null}

                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <label htmlFor="input1">Название</label>
                                <input
                                    ref="name"
                                    type="text"
                                    id="input1"
                                    className="form-control"
                                    placeholder="Название" />
                            </FormGroup>

                            <FormGroup>
                                <label>Ключевые теги</label>

                                <input
                                    ref="tagLine"
                                    type="text"
                                    className="form-control"
                                    placeholder="Слова через запятую" />
                            </FormGroup>

                            <FormGroup>
                                <label htmlFor="textarea1">Описание</label>
                                <textarea
                                    ref="desc"
                                    id="textarea1"
                                    className="form-control"
                                    rows="3" />
                            </FormGroup>

                            <FormGroup>
                                <label htmlFor="input2">Срок сдачи проекта</label>
                                <Input
                                    ref="deadline"
                                    type="date"
                                    id="input2"
                                    className="form-control"/>
                            </FormGroup>

                            <FormGroup>
                                <label htmlFor="input3">Предлагаемая оплата</label>
                                <input
                                    ref="price"
                                    type="number"
                                    id="input3"
                                    className="form-control"
                                    placeholder="Сумма в рублях" />
                            </FormGroup>

                            <div className="form-row align-items-center justify-content-end">
                                <div className="col-auto">
                                    <button type="submit" className="btn btn-primary mb-2">Создать</button>
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
        )
    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('profiles');

    return {
        user: Meteor.user(),
        hasProfile: Profiles.find({userId: Meteor.userId()}).count()
    };
})(TaskCreatePage);
