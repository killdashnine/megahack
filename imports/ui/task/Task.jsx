import React, { Component } from 'react';

import {Link} from "react-router-dom";

import { Badge } from 'reactstrap';
import TagsRaw from "../TagsRaw";

export default class TaskItem extends Component {
    render() {
        let task = this.props.task;

        if(!task)
            return <div>404, task not found</div>;

        return (
            <div className="card d-block">
                <div className="card-body">
                    <div className="dropdown card-widgets">
                        <a href="javascript:void(0);" className="dropdown-toggle arrow-none" data-toggle="dropdown"
                           aria-expanded="false">
                            <i className="fas fa-ellipsis-h" />
                        </a>
                        {/*<div className="dropdown-menu dropdown-menu-right" x-placement="bottom-end"*/}
                             {/*style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-142px, 20px, 0px);">*/}
                            {/*<!-- item-->*/}
                            {/*<a href="javascript:void(0);" className="dropdown-item"><i className="fas mr-1 fa-pen" />Edit</a>*/}
                            {/*<!-- item-->*/}
                            {/*<a href="javascript:void(0);" className="dropdown-item"><i className="fas mr-1 fa-trash-alt" />Delete</a>*/}
                        {/*</div>*/}
                    </div>

                    <h3 className="mt-0">
                        {task.name}
                    </h3>

                    <TagsRaw tags={task.tags}/>

                    <h5>Описание заказа</h5>

                    <div key={"description"} className="text-muted mb-4">
                        {task.desc}
                    </div>

                    <div key={"info"} className="row">
                        { task.createdAt instanceof Date
                            ? <div key={"createdAt"} className="col-md-4">
                                <div className="mb-4">
                                    <h5>Дата размещения</h5>
                                    <p>{task.createdAt.toLocaleDateString('ru', {
                                        year: 'numeric',
                                        month: 'long',
                                        day: 'numeric',
                                        timezone: 'UTC',
                                        hour: 'numeric',
                                        minute: 'numeric',
                                    })}</p>
                                </div>
                            </div>
                            : '' }

                        { task.deadline instanceof Date
                            ? <div key={"deadline"} className="col-md-4">
                                <div className="mb-4">
                                    <h5>Срок сдачи</h5>
                                    <p>{task.deadline.toLocaleDateString('ru', {
                                        year: 'numeric',
                                        month: 'long',
                                        day: 'numeric',
                                        timezone: 'UTC',
                                        hour: 'numeric',
                                        minute: 'numeric',
                                    })}</p>
                                </div>
                            </div>
                            : '' }
                        { task.price
                            ? <div key={"price"} className="col-md-4">
                                <div className="mb-4">
                                    <h5>Бюджет</h5>
                                    <p>{task.price} р</p>
                                </div>
                            </div>
                            : '' }
                    </div>
                </div>
            </div>
        );
    }
}
