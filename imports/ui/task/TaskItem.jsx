import React, { Component } from 'react';

import {Link} from "react-router-dom";

import { Badge } from 'reactstrap';

import TagsRaw from '../TagsRaw';

export default class TaskItem extends Component {
    render() {
        let task = this.props.task;
        // let desc = task.desc;

        if(!task)
            return <div>404, task not found</div>;

        return (
            <div className="card">
                <div className="card-body">
                    <div className="float-right">
                        {task.price
                            ? <h5 className="mt-0">{task.price} р</h5>
                            : ''}
                    </div>
                    <Link className="h5 text-dark" to={'/task/' + task._id}>{task.name}</Link>

                    {/*<p>{desc.length > 50 ? desc.slice(0, 50) + '...' : desc}</p>*/}

                    <ul className="mb-0 list-inline">
                        <li className="list-inline-item mr-3">
                            Откликов { Array.isArray(task.solutions) ? task.solutions.length : 0 }
                        </li>
                        <li className="list-inline-item">
                            Просмотров 25
                        </li>
                    </ul>

                    <TagsRaw tags={task.tags}/>
                </div>
            </div>
        );
    }
}
