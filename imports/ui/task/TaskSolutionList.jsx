import React, {Component} from 'react';

import TaskSolution from './TaskSolution';

export default class TaskSolutionList extends Component {
    render() {
        let solutions = this.props.solutions || [];
        let extended = this.props.extended || false;

        return (
            <div className="card">
                <div className="card-body">
                    <h4 className="mt-0 mb-3">Отклики</h4>
                    {solutions.reverse().map(solution =>
                        <TaskSolution
                            key={solution._id}
                            solution={solution}
                            extended={extended}
                            taskId={this.props.taskId}
                        />)}
                </div>
            </div>
        );
    }
}
