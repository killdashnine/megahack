import React, {Component} from 'react';

import TaskItem from './TaskItem';

export default class TaskList extends Component {
    render() {
        return (
            <div>
                {/*this.props.incompleteCount > 0 ?
                    <ListGroupItem color="info">
                        Incomplete tasks count: {this.props.incompleteCount}
                    </ListGroupItem> : ''*/}
                {this.props.tasks.map(task =>
                    <TaskItem
                        key={task._id}
                        task={task}
                    />)}
            </div>
        );
    }
}
