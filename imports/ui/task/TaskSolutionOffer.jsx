import { Meteor } from 'meteor/meteor';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import { Button } from 'reactstrap';

import { autobind } from 'core-decorators';
import classNames from 'classnames';

export default class TaskSolutionOffer extends Component {

    @autobind
    handleSubmit(event) {

        event.preventDefault();

        // Find the text field via the React ref
        const text = ReactDOM.findDOMNode(this.refs.messageInput).value.trim();
        const help = ReactDOM.findDOMNode(this.refs.helpInput).checked;

        if(text.length > 0)
            Meteor.call('tasks.solutions.insert', this.props.task._id, text, help);

        // Clear form
        ReactDOM.findDOMNode(this.refs.messageInput).value = '';
    }

    render() {

        let task = this.props.task;

        let offered = (task.solutions || []).filter(solution => (solution.contractor||{})._id === Meteor.userId()).length;

        if(offered) return '';

        return (
            <div className="card">
                <div className="card-body">
                    <h4 className="mt-0 mb-3">Оставьте свой отклик</h4>

                    <form onSubmit={this.handleSubmit}>

                        <textarea className="form-control form-control-light mb-2" placeholder="Напишите отклик"
                                  id="example-textarea" ref={"messageInput"} rows="3" />

                        <div className="form-row align-items-center justify-content-end">
                            <div className="col-auto">
                                <div className="custom-control custom-checkbox mb-2">
                                    <input ref="helpInput" type="checkbox" className="custom-control-input" id="needHelp" />
                                    <label className="custom-control-label" htmlFor="needHelp">Нужна помощь куратора</label>
                                </div>
                            </div>
                            <div className="col-auto">
                                <button type="submit" className="btn btn-primary mb-2">Отправить</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}
