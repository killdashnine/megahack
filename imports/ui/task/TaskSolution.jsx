import React, { Component } from 'react';

import {Link} from "react-router-dom";

import { Badge } from 'reactstrap';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Profiles} from "../../api/profiles";
import {Tasks} from "../../api/tasks";

import Workflow from '../Workflow';

import { autobind } from 'core-decorators';

// function inlineTry(t, c){
//     try{  return t();  } catch{  if(typeof(c)==='function') return c(); return c;  }
// }

class TaskSolution extends Component {

    @autobind
    chooseContractor(event) {

        event.preventDefault();

        Meteor.call('tasks.accept', this.props.task._id, this.props.solution._id, Meteor.userId());

    }

    render() {
        let solution = this.props.solution || {};
        let extended = this.props.extended || {};
        let task = this.props.task || {};
        let user = Meteor.user();
        let profile = this.props.profile || {};

        if(!solution)
            return <div>404, solution not found</div>;

        console.log();

        // console.log(user, task.customer, (task.customer || {})._id, user._id);

        return (
            <div className="media mt-2">
                <img
                    className="mr-3 avatar-sm rounded-circle"
                    src={"https://ui-avatars.com/api/?name=" + (this.props.contractor || {}).name}
                    alt="Описание" />
                <div className="media-body">
                    <h5 className="mt-0">
                        { (this.props.contractor || {}).name }
                        { solution.accepted ? ' - Исполнитель' : '' }
                    </h5>
                    <p className="mb-1">{ solution.message }</p>
                    { user && (user._id === solution.contractor._id || profile.role === 'mentor') && solution.helpers
                        ? <div className="small text-right text-muted">
                            <i className="fas fa-check-circle" />
                            Нужна помощь наставника
                        </div>
                        : '' }

                    { user && ((solution || {}).accepted || (this.props.task.tags || []).includes('обучение')) && [
                        (solution.contractor || {})._id,
                        (this.props.task.customer || {})._id,
                        (solution.mentor || {})._id
                    ].includes(user._id)
                        ? <Workflow
                            points={solution.points}
                            taskId={this.props.taskId}
                            solutionId={solution._id}
                            canEdit={ user && (user._id === (solution.contractor || {})._id || user._id === (solution.mentor || {})._id) }
                        />
                        : '' }

                    { user && task.customer && task.customer._id === user._id
                        ? <div className="text-right">
                            <div className="btn-group">
                                <button className="btn btn-sm btn-outline-success btn-rounded" onClick={this.chooseContractor}>Выбрать исполнителя</button>
                            </div>
                        </div>
                        : '' }

                </div>
            </div>
        );

    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('profiles');

    let contractorId = (props.solution.contractor || {})._id;
    let mentorId = (props.solution.mentor || {})._id;
    let taskId = props.taskId;

    let contractor = Profiles.findOne({
        userId: contractorId
    });

    let mentor = Profiles.findOne({
        userId: mentorId
    });

    return {
        contractor,
        mentor,
        task: Tasks.findOne({_id: taskId}),
        profile: Profiles.findOne({userId: Meteor.userId()})
    };

})(TaskSolution);

