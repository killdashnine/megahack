import React, { Component } from 'react';

import {BrowserRouter as Router, Route, Link} from "react-router-dom";

import TopNavigation from './TopNavigation.jsx';
import Landing from './pages/Landing'
import TasksPage from './pages/TasksPage';
import TaskPage from './pages/TaskPage'
import TaskCreatePage from './pages/TaskCreatePage'
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Redirect} from "react-router";
import ProfileEditPage from "./pages/ProfileEditPage";
import {Profiles} from "../api/profiles";

class App extends Component {

    componentDidCatch(error, info) {
        console.log(error, info);
    }

    render() {

        let user = this.props.user;
        let profile = this.props.profile;

        return (
            <Router>
                <div>
                    <TopNavigation />
                    <div className="container">
                        <Route
                            path='/'
                            exact={true}
                            component={TasksPage}
                        />
                        <Route
                            path='/task'
                            exact={true}
                            component={TasksPage}
                        />

                        <Route
                            path='/task/:id'
                            exact={true}
                            component={TaskPage}
                        />

                        <Route
                            path='/task-create'
                            exact={true}
                            component={TaskCreatePage}
                        />

                        <Route
                            path='/profile'
                            exact={true}
                            component={ProfileEditPage}
                        />

                        {/* user && !profile && window.location.href !== '/profile' ? // this.props.location.pathname
                            <Redirect to='/profile' />
                            : ''
                        */}

                    </div>
                </div>
            </Router>
        );
    }
}

export default withTracker(props => {

    Meteor.subscribe('tasks'); // For what ???
    Meteor.subscribe('profiles');

    return {
        user: Meteor.user(),
        profile: Profiles.findOne({userId: Meteor.userId()})
    };
})(App);

