import React, {Component} from 'react';

import { Button, CardFooter, Badge, Card, CardBody, CardHeader } from 'reactstrap';

import {autobind} from 'core-decorators';

export default class TagFilterSide extends Component {

    state = { showCancel: false };

    @autobind
    applyFilter(tag){
        this.setState({showCancel: true});

        const cb = this.props.onTagChosen;

        if(typeof cb === 'function') cb(tag);
    }

    @autobind
    cancelFilter(){
        this.setState({showCancel: false});

        const cb = this.props.onTagChosen;

        if(typeof cb === 'function') cb(null);
    }

    render() {
        const tags = this.props.tags;
        const cb   = this.props.onTagChosen;

        return (
            <Card>
                <CardHeader>
                    Фильтр по тегам
                </CardHeader>
                <CardBody>
                    { tags.map(tag =>
                        <Badge
                            color="primary"
                            className="mr-1"
                            key={tag}
                            href="#"
                            onClick={() => this.applyFilter(tag)} >

                            {tag}

                        </Badge>) }
                </CardBody>
                { this.state.showCancel
                    ? <CardFooter>
                        <Button block onClick={this.cancelFilter}>Отмена</Button>
                    </CardFooter>
                    : '' }
            </Card>
        );
    }
}
