import React, {Component} from 'react';

import { Button, CardFooter, Badge, Card, CardBody, CardHeader } from 'reactstrap';

import {autobind} from 'core-decorators';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Profiles} from "../../api/profiles";

class ProfileSide extends Component {

    render() {
        const profile = this.props.profile || {};
        const header = this.props.header || '';

        return (
            <Card>
                <CardHeader>
                    { String(header) }
                </CardHeader>
                <CardBody>
                    <div className="text-center">
                        <img src={"https://ui-avatars.com/api/?name=" + profile.name} style={{height: "100px"}} alt=""
                             className="rounded-circle img-thumbnail" />
                            <div className="h4">{profile.name}</div>
                            <div className="small">{profile.desc}</div>
                    </div>
                </CardBody>
            </Card>
        );
    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('profiles');

    let profile = props.profile || Profiles.findOne({
        userId: props.userId
    });

    return {
        profile
    };
})(ProfileSide);
