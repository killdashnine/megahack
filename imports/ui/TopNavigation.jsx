import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import {Link} from "react-router-dom";
import {autobind} from 'core-decorators';
import AccountsUIWrapper from "./AccountsUIWrapper";
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Profiles} from "../api/profiles";

import Blaze from 'meteor/gadicc:blaze-react-component';

class TopNavigation extends React.Component {

    state = {
        isOpen: false
    };

    @autobind
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <header>

                <Navbar className="navbar-custom topnav-navbar" tag="div">
                    <div className="container">

                        <Link to="/" className="topnav-logo">
                            <span className="topnav-logo-lg">
                                <img src="https://evland.ru/wp-content/themes/everland/img/logo.png" alt="" />
                            </span>
                                <span className="topnav-logo-sm">
                                <img src="https://evland.ru/wp-content/themes/everland/img/logo.png" alt="" height="16" />
                            </span>
                        </Link>

                        <ul className="list-unstyled topbar-right-menu float-right mb-0" style={{"display": "flex", "alignItems": "center"}}>

                            <li>
                                <Blaze template="loginButtons" align="right" />
                            </li>

                            { this.props.profile
                                ? <UncontrolledDropdown className="notification-list" tag="li">
                                    <DropdownToggle className="nav-link dropdown-toggle nav-user arrow-none mr-0" tag="a">
                                <span className="account-user-avatar">
                                    <img src={"https://ui-avatars.com/api/?name=" + this.props.profile.name} alt="user-image" className="rounded-circle" />
                                </span>
                                        <span>
							        <span className="account-user-name">{this.props.profile.name}</span>
							        <span className="account-position">{this.props.profile.role === 'mentor' ? "Наставник" : "Пользователь"}</span>
						        </span>
                                    </DropdownToggle>
                                    <DropdownMenu right animated="true" className="profile-dropdown">
                                        <DropdownItem className="notify-item">
                                            <i className="mdi mdi-account-circle">
                                            </i>
                                            <span>Мой аккаунт</span>
                                        </DropdownItem>
                                        <DropdownItem>
                                            <i className="mdi mdi-account-settings-variant">
                                            </i>
                                            <span>Настройки</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                                : '' }
                        </ul>
                        <NavbarToggler onClick={this.toggle} tag="a" className="navbar-toggle">
                            <div className="lines">
                                <span />
                                <span />
                                <span />
                            </div>
                        </NavbarToggler>
                    </div>
                </Navbar>

                <div className="topnav mb-4">
                    <div className="container">
                        <Navbar dark expand="lg" className="topnav-menu">
                            <Collapse isOpen={this.state.isOpen} navbar className="active" tag="div">
                                <Nav navbar>
                                    <NavItem>
                                        <Link className="nav-link" to="/task">Заказы</Link>
                                    </NavItem>
                                    { this.props.profile && this.props.profile.role === "mentor"
                                        ? <NavItem>
                                            <Link className="nav-link" to="/">Заявки</Link>
                                        </NavItem>
                                        : '' }
                                    <NavItem>
                                        <Link className="nav-link" to="/task-create">Разместить заказ</Link>
                                    </NavItem>
                                    <NavItem>
                                        <Link className="nav-link" to="/profile">Профиль</Link>
                                    </NavItem>
                                </Nav>
                            </Collapse>
                        </Navbar>
                    </div>
                </div>

            </header>
        );
    }
}

export default withTracker(props => {

    const handle = Meteor.subscribe('profiles');

    let profile = Profiles.findOne({
        userId: Meteor.userId()
    });

    return {
        profile
    };
})(TopNavigation);
