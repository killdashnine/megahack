import React, {Component} from 'react';

import { Button, CardFooter, Badge, Card, CardBody, CardHeader } from 'reactstrap';

import {autobind} from 'core-decorators';

export default class TagFilterSide extends Component {

    render() {
        const tags = this.props.tags;

        return (
            <div className="mt-1">
                { Array.isArray(tags)
                    ? tags.map(tag => <Badge key={tag} className="mr-1 mb-1" color="primary">{tag}</Badge>)
                    : ''}
            </div>
        );
    }
}
