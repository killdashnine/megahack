import React, {Component} from 'react';

import { Button, Badge, Form, FormGroup, InputGroup, Input, InputGroupAddon, InputGroupText, InputGroupButton } from 'reactstrap';

import {autobind} from 'core-decorators';
import ReactDOM from "react-dom";
import {Meteor} from "meteor/meteor";

import classNames from 'classnames';

export default class Workflow extends Component {

    @autobind
    handleReport(event) {

        event.preventDefault();

    }

    @autobind
    preventSubmit(event) {

        event.preventDefault();
    }

    @autobind
    handleSubmit(event) {

        let form = event.target.closest('.goal-form');

        let textArea = form.querySelector('textarea[name="goalInput"]');
        let date     = form.querySelector('input[name="dateInput"]');
        let time     = form.querySelector('input[name="timeInput"]');

        let goal = textArea.value.trim();
        let datetime = new Date(`${date.value} ${time.value}`);

        // let onSubmitCheckpoint = this.props.onSubmitCheckpoint || (()=>{});

        if(goal.length > 0)
            // onSubmitCheckpoint(goal, datetime);
            Meteor.call('tasks.solutions.points.insert', this.props.taskId, this.props.solutionId, goal, datetime);

        textArea.value = '';
    }

    render() {
        const points = this.props.points || [];
        // this.props.onSubmitCheckpoint;
        // this.props.onReportCheckpoint;

        console.log(points);

        return (
            <div>
                <div className="m-pointline-block">
                    { points.length ?
                        <div className="m-pointline">
                            <div className="m-pointline__start">0%</div>
                        </div>
                        : '' }

                    <div className="m-pointline-block__wrapper">

                        { points
                            .sort((p1, p2) => p1.time.getTime() - p2.time.getTime())
                            .map(point => (
                            <div key={point._id}
                                className={classNames({
                                "m-pointline-item": 1,
                                "active": point.time.getTime() < (new Date()).getTime() })}>
                                <div className="m-pointline-item__pointer">
                                    <div className="m-pointline-item__val">{ typeof point.percent === 'number' ? point.percent+"%" : '' }</div>
                                </div>
                                <div className="m-pointline-item__title">
                                    <h4>
                                        { point.goal || '' }
                                        <Badge className="float-right">
                                            { point.time.toLocaleDateString('ru', {
                                                year: 'numeric',
                                                month: 'long',
                                                day: 'numeric',
                                                timezone: 'UTC',
                                                hour: 'numeric',
                                                minute: 'numeric',
                                            }) }
                                        </Badge>
                                    </h4>
                                </div>
                                <div className="m-pointline-item__content">
                                    { point.report || '' }
                                </div>
                                {/*<div className="m-pointline-item__btn">*/}
                                    {/*<div className="btn-group" role="group" aria-label="Basic example">*/}
                                        {/*<button type="button" className="btn btn-outline-primary btn-rounded">Изменить*/}
                                        {/*</button>*/}
                                        {/*<button type="button" className="btn btn-outline-success btn-rounded">Отметить*/}
                                            {/*как выполненное*/}
                                        {/*</button>*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                            </div>
                        )) }
                    </div>
                    {/*<div className="m-pointline-block__btn">*/}
                        {/*<button type="button" className="btn btn-primary btn-block">Добавить контрольную точку*/}
                        {/*</button>*/}
                    {/*</div>*/}
                </div>

                { this.props.canEdit
                    ? <Form onSubmit={this.preventSubmit} className="goal-form">
                        <FormGroup>
                            <h3>Новая контрольная точка</h3>
                            <textarea className="form-control form-control-light mb-2"
                                      placeholder="Кратко опишите цель"
                                      name="goalInput"
                                      rows="3" />
                            <InputGroup>
                                <Input type="time" name="timeInput"/>
                                <Input type="date" name="dateInput"/>
                                <InputGroupAddon addonType="append">
                                    <Button onClick={this.handleSubmit}>Создать</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </FormGroup>
                    </Form>
                    : '' }
            </div>
        );
    }
}
