import {Mongo} from "meteor/mongo";

import {parse} from 'path';

import _ from 'lodash';

export default class Migration {

    constructor(version, name) {
        if (version !== undefined) this.version = version;
        if (name !== undefined) this.name = name;
    }

    /**
     * Mark migration changes on documents by given ids at given collections
     * @param {Object.<string, string[]>} targets
     */
    markChanges(targets) {

        for (let name in targets) {

            if (!targets.hasOwnProperty(name)) continue;

            let ids = [... new Set(_.castArray(targets[name]))];

            for (let id of ids)
                Mongo.Collection.get(name).update({_id: id}, {$push: {[Migration.marker]: this.version}});
        }
    }

    /**
     * Unmark migration changes at given collections
     * @param { string[] } targets Array of collection names
     */
    unmarkChanges(targets) {

        for (let name of _.castArray(targets)) {

            Mongo.Collection.get(name).update({[Migration.marker]: this.version}, {$pull: {[Migration.marker]: this.version}}, {multi: true});
        }
    }

    static revert(migration, newVersion) {

        let instance = new migration();

        return class RevertedMigration extends Migration {

            version = newVersion;

            name = `Reverting migration #${instance.version}`;

            up() {
                migration.prototype.down.apply(this, arguments);
            }

            down() {
                migration.prototype.up.apply(this, arguments);
            }
        };
    }

    static merge(migrations, newVersion) {

        let instances = migrations.map(m => new m());

        return class MergedMigration extends Migration {

            version = newVersion;

            name = `Applying merged migrations ${instances.map(i => '#' + i.version).join(',')}`;

            up() {
                migrations.forEach(m => m.prototype.up.apply(this, arguments));
            }

            down() {
                migrations.forEach(m => m.prototype.down.apply(this, arguments));
            }
        };
    }

    static parseVersion(filename) {
        return Number(parse(filename).name);
    }

    static marker = 'migrationVersions';
}
