import {Tasks} from '../../../imports/api/tasks';
import {Profiles} from '../../../imports/api/profiles';
import BaseMigration from '../migration';
import {Accounts} from 'meteor/accounts-base';
import {Random} from 'meteor/random';

import {shiftDate} from '../../../imports/util/relativeTime';

function altUser(username){

    let user = Accounts.findUserByUsername(username);

    let profile = Profiles.findOne({userId: user._id});

    return {_id: user._id, name: profile.name};
}

export default class Migration extends BaseMigration {

    version = BaseMigration.parseVersion(__filename);

    name = 'Mock tasks';

    tasks = () => [{
        name: 'Нарисовать единорога',
        desc: 'Фактически, нужно всего лишь нарисовать лошадь с рогом.',
        deadline: new Date(),
        customer: altUser('sample-mentor-1'),
        tags: ["дизайн", "работа руками", "рисование", "обучение"],
        createdAt: shiftDate(new Date(), -10),

        solutions: [{
            _id: Random.id(),
            mentor: altUser('sample-mentor-1'),
            contractor: altUser('sample-contractor-1'),
            message: "Хочу попробовать себя в рисовании",
            points: [{
                _id: Random.id(),
                percent: 15,
                time: shiftDate(new Date(), -1),
                goal: 'Рог',
                report: "Сделал, но больше похож на антенну",
            },{
                _id: Random.id(),
                percent: 33,
                time: shiftDate(new Date(), 0),
                goal: 'Голова и шея',
                report: "Вроде бы получилось"
            },{
                _id: Random.id(),
                percent: 66,
                time: shiftDate(new Date(), 1),
                goal: 'Тело и хвост',
                // report: "Ellipse and bunch of lines."
            },]
        },{
            _id: Random.id(),
            accepted: true,
            mentor: altUser('sample-mentor-1'),
            contractor: altUser('sample-contractor-2'),
            message: "Давайте просто сделаем это",
            points: [{
                _id: Random.id(),
                percent: 10,
                time: shiftDate(new Date(), 0, -5),
                goal: 'Рог!',
                report: "Сделано!"
            },{
                _id: Random.id(),
                percent: 20,
                time: shiftDate(new Date(), 0, -4),
                goal: 'Голова!',
                report: "Готово!"
            },{
                _id: Random.id(),
                percent: 40,
                time: shiftDate(new Date(), 0, -3),
                goal: 'Шея!',
                report: "Успех!"
            },{
                _id: Random.id(),
                percent: 60,
                time: shiftDate(new Date(), 0, -2),
                goal: 'Тело!',
                report: "Есть!"
            },{
                _id: Random.id(),
                percent: 80,
                time: shiftDate(new Date(), 0, -1),
                goal: 'Хвост!',
                report: "Осилил."
            },{
                _id: Random.id(),
                percent: 100,
                goal: 'Legs!',
                time: new Date(),
            },]
        },]
    },{
        name: 'Нарисовать домик',
        desc: 'Изобразить дом с дверью, окном и печной трубой',
        deadline: shiftDate(new Date(), 1),
        customer: altUser('sample-customer-1'),
        tags: ["дизайн", "карандаши", "рисование"],
        price: 1000,
        createdAt: shiftDate(new Date(), -2),

        solutions: [{
            _id: Random.id(),
            contractor: altUser('sample-contractor-1'),
            mentor: altUser('sample-mentor-1'),
            helpers: {
                [Accounts.findUserByUsername('sample-mentor-1')]: 1
            },
            message: "Я - тот, кого Вы ищете!",
        },{
            _id: Random.id(),
            contractor: altUser('sample-contractor-2'),
            accepted: true,
            message: "Hold my beer.",
        },]
    },];

    up() {
        let ids = this.tasks().map(task => Tasks.insert(task));

        this.markChanges({'tasks': ids});
    }

    down() {

        Tasks.remove({[BaseMigration.marker]: this.version});
    }
}
