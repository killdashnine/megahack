import BaseMigration from '../migration';

import {Accounts} from 'meteor/accounts-base';

export default class Migration extends BaseMigration {

    version = BaseMigration.parseVersion(__filename);

    name = 'Mock users for task view';

    users = [{
        username: 'sample-mentor-1',
        password: 'sample'
    },{
        username: 'sample-customer-1',
        password: 'sample'
    },{
        username: 'sample-contractor-1',
        password: 'sample'
    },{
        username: 'sample-contractor-2',
        password: 'sample'
    },];

    up() {

        const userIds = this.users.map(user => Accounts.createUser({
            username: user.username,
            password: user.password}));
            // password: Meteor.isProduction ? '' : user.password}));

        this.markChanges({'users': userIds});
    }

    down() {

        Meteor.users.remove({[BaseMigration.marker]: this.version});
    }
}
