const migrateVersion = process.env.MIGRATION_VERSION || "latest";
// const migrateVersion = process.env.ADMIN_PASSWORD || 0;

export default function migrate() {

    let modules;

    try {
        modules = [
            require('./100'),
            require('./101'),
            require('./102'),
        ];
    }
    catch(error){
        console.error('Some migrations cannot be loaded');
        process.exit(1);
    }

    modules.forEach(module => {
        const Migration = module.default;
        Migrations.add(new Migration());
    });

    Migrations.migrateTo(migrateVersion);
}
