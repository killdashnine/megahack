import {Profiles} from '../../../imports/api/profiles';
import BaseMigration from '../migration';
import {Accounts} from 'meteor/accounts-base';
import _ from 'lodash';

import {relativeMillis, week} from '../../../imports/util/relativeTime';

export default class Migration extends BaseMigration {

    version = BaseMigration.parseVersion(__filename);

    name = 'Mock data for task view';

    profiles = [{
        username: 'sample-mentor-1',
        timePeriods: [
            [
                relativeMillis(week['saturday'], 17, 0),
                relativeMillis(week['saturday'], 20, 0)], [
                relativeMillis(week['sunday'],   17, 0),
                relativeMillis(week['sunday'],   20, 0)
            ]],
        contact: "https://vk.com/killdashnine",
        role: 'mentor',
        name: 'Магистр Йода',
        desc: "Мастер светового меча"
    },{
        username: 'sample-customer-1',
        timePeriods: [
            [
                relativeMillis(week['saturday'], 18, 0),
                relativeMillis(week['saturday'], 24, 0)], [
                relativeMillis(week['sunday'],   18, 0),
                relativeMillis(week['sunday'],   24, 0)
            ]],
        contact: "https://t.me/killdashnine",
        role: 'customer',
        name: 'Билл Гейтс',
        desc: "Мультимиллионер"
    },{
        username: 'sample-contractor-1',
        timePeriods: [
            [
                relativeMillis(week['saturday'], 12, 0),
                relativeMillis(week['saturday'], 22, 0)], [
                relativeMillis(week['sunday'],   12, 0),
                relativeMillis(week['sunday'],   22, 0)
            ]],
        contact: "email:quqtus@gmail.com",
        role: 'contractor',
        name: 'Иван Иванов',
        desc: "Студент"
    },{
        username: 'sample-contractor-2',
        timePeriods: [
            [
                relativeMillis(week['saturday'], 0, 0),
                relativeMillis(week['saturday'], 24, 0)], [
                relativeMillis(week['sunday'],   0, 0),
                relativeMillis(week['sunday'],   24, 0)
            ]],
        contact: "email:geforce.kolya@mail.ru",
        role: 'contractor',
        name: 'Петр Петров',
        desc: "Школьник"
    },];

    up() {
        let ids = this.profiles
            .map(profile => _.merge({ userId: Accounts.findUserByUsername(profile.username)._id },
                _.omit(profile, 'username')))
            .map(profile => Profiles.insert(profile));

        this.markChanges({'profiles': ids});
    }

    down() {

        Profiles.remove({[BaseMigration.marker]: this.version});
    }
}
