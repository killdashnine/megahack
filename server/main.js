import {Meteor} from 'meteor/meteor';
import migrate from './db/migrations';

import {Profiles} from '../imports/api/profiles'
import {Tasks} from '../imports/api/tasks'

// import collections here

Meteor.startup(() => {
    migrate();
});
